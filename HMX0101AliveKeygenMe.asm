; Not only modified crypto and magic this time, but compression stuff too!
; HMX0101 is using the inverse Burrows-Wheeler transformation when parsing
; the last 60 chars of the serial, so we have to make the transformation on
; this side. Since no implementation in MASM32 was to be found on the net
; I had to write it from scratch. That's what kept this keygen from appearing
; for a few days (I didn't have time to sit down and write). In the end it took
; a mere couple hours to have it workig lol
; On the bright side, this gave me time to think of a couple of optimizations
; that reduced the memory requirements to a mere (StrLen*2)-1+(StrLen*4) bytes.
; The cypto side of things: modified 80bits ElGamal using 2 random parameters
; instead of just one by Xiaofei Li, Xuanjing Shen and Haipeng Chen.
; Standard MD5 is used to calculate the hash of the username.
; All in all a fun keygenme, had to google a bit to get all the correct infos
; and to get BWT right, but it was all in good fun. Also, I had the chance to
; defeat the KeygenMe in two different ways, first solving the DLP for
; every serial (done once using MAGMA to check if the theory worked),
; then using the "real" algo. Quite entertaining :)

.code
Gen_HMX0101AliveKeygenMe proc in_username: DWORD, out_serial: DWORD
    invoke RtlZeroMemory, offset HMXAlive_TmpStr, sizeof HMXAlive_TmpStr

    invoke MD5Init
    invoke StrLen, in_username
    invoke MD5Update, in_username, eax
    invoke MD5Final
    invoke HexEncode, eax, MD5_DIGESTSIZE, offset HMXAlive_TmpStr
    invoke szLower, offset HMXAlive_TmpStr

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigTmp, eax
    invoke bnCreate
    mov BigM, eax
    Invoke bnCreate
    mov BigP, eax
    invoke bnCreate
    mov BigK, eax
    invoke bnCreate
    mov BigG, eax
    invoke bnCreate
    mov BigR, eax
    invoke bnCreate
    mov BigS, eax
    invoke bnCreate
    mov BigX, eax

    invoke bnFromBytesEx, offset HMXAlive_TmpStr, MD5_DIGESTSIZE*2, BigM, FALSE
    invoke bnFromHex, offset HMXAlive_P, BigP
    invoke bnFromHex, offset HMXAlive_G, BigG
    invoke bnFromHex, offset HMXAlive_X, BigX

    invoke bnMod, BigM, BigP, BigM
    invoke bnRsaGenPrime, BigK, 79
    invoke bnRsaGenPrime, BigTmp, 79

    invoke bnModExp, BigG, BigK, BigP, BigR
    invoke bnModExp, BigG, BigTmp, BigP, BigS
    invoke bnMul, BigX, BigR, BigX
    invoke bnMul, BigK, BigS, BigK
    invoke bnMul, BigTmp, BigM, BigTmp
    invoke bnAdd, BigTmp, BigK
    invoke bnAdd, BigTmp, BigX
    invoke bnDec, BigP
    invoke bnMod, BigTmp, BigP, BigM

    ; Those three shifts are a workaround for drizz's library,
    ; not part of the modified ElGamal algo
    invoke bnShl, BigR, 16
    invoke bnShl, BigS, 16
    invoke bnShl, BigM, 16
    invoke bnToHex, BigR, offset HMXAlive_TmpStr
    invoke bnToHex, BigS, offset HMXAlive_TmpStr+20
    invoke bnToHex, BigM, offset HMXAlive_TmpStr+40
    mov dword ptr ds:[HMXAlive_TmpStr+60], 00h

    invoke bnDestroy, BigX
    invoke bnDestroy, BigS
    invoke bnDestroy, BigR
    invoke bnDestroy, BigG
    invoke bnDestroy, BigK
    invoke bnDestroy, BigP
    invoke bnDestroy, BigM
    invoke bnDestroy, BigTmp
    invoke bnFinish

    invoke szCatStr, out_serial, offset HMXAlive_3Chars
    invoke BWT_Transform, offset HMXAlive_TmpStr, offset HMXAlive_TmpStr, 60
    mov edx, eax
    inc edx
    mov eax, out_serial
    add eax, 3
    invoke wsprintf, eax, offset HMXAlive_Decwsp, edx
    invoke szCatStr, out_serial, offset HMXAlive_TmpStr
    Ret
Gen_HMX0101AliveKeygenMe endp
