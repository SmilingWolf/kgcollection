; BWT_Transform implemented in pure MASM32 bitchez!
; We use a pointer table instead of pure strings here,
; so pointers can be swapped and rotations can be simulated
; We use BubbleSort to sort the strings. I know its slow,
; but it keeps things simple and tidy
; - SmilingWolf, 2016

BWT_BubbleSort proto InputTable: DWORD, StringLength: DWORD
BWT_StrCmp proto String1: DWORD, String2: DWORD, StringLength: DWORD

.code
; Input should be quite self-explanatory
; Please note that since we are working with temporary memory buffers
; InputString and OutputString can be the same
; Output: index of the original string inside the array of rotated strings
BWT_Transform proc InputString: DWORD, OutputString: DWORD, StringLength: DWORD
    local PointersTable: DWORD
    local RotString: DWORD
    local OrigStringIndex: DWORD

    ; Alloc the space for the rotated strings
    ; Thanks to a simple optimization we only need (StringLength*2)-1 bytes
    mov eax, StringLength
    add eax, StringLength
    dec eax
    invoke VirtualAlloc, NULL, eax, MEM_COMMIT or MEM_RESERVE, PAGE_READWRITE
    mov RotString, eax

    ; Copy the "rotated" string into the buffer
    ; We will be emulating string rotation using the pointers actually
    mov esi, InputString
    mov edi, RotString
    mov ecx, StringLength
    rep movsb
    mov esi, InputString
    mov edi, RotString
    mov ecx, StringLength
    add edi, ecx
    dec ecx
    rep movsb

    ; Alloc the space for the pointers to the "rotated" strings
    ; Since pointer = DWORD, we need to alloc StringLength*4 bytes
    mov eax, StringLength
    imul eax, 4
    invoke VirtualAlloc, NULL, eax, MEM_COMMIT or MEM_RESERVE, PAGE_READWRITE
    mov PointersTable, eax

    ; Fill the pointers table adding 1 to the string pointer each time
    mov eax, PointersTable
    xor ecx, ecx
    mov edx, StringLength
    mov esi, RotString
    .while ecx < edx
        mov dword ptr ds:[eax+ecx*4], esi
        inc esi
        inc ecx
    .endw

    ; Now the real fun begins: we have to sort the pointers based on
    ; lexicographical order of the strings
    ; YES I KNOW BUBBLESORT IS SHIT, but it's fast and easy to implement
    invoke BWT_BubbleSort, PointersTable, StringLength

    ; Find the original string among the rotated ones,
    ; give in output its index inside the ordered array
    xor eax, eax
    inc eax
    mov ebx, PointersTable
    xor ecx, ecx
    mov edx, StringLength
    .while eax != 00h && ecx < edx
        invoke BWT_StrCmp, InputString, dword ptr ds:[ebx+ecx*4], StringLength
        inc ecx
    .endw
    dec ecx
    mov OrigStringIndex, ecx

    ; Build the BTWed string
    mov ebx, PointersTable
    xor ecx, ecx
    mov edx, StringLength
    mov edi, OutputString
    .while ecx < edx
        mov eax, dword ptr ds:[ebx]
        add eax, edx
        dec eax
        movzx eax, byte ptr ds:[eax]
        mov byte ptr ds:[edi], al
        add ebx, 4
        inc ecx
        inc edi
    .endw

    invoke VirtualFree, PointersTable, 00h, MEM_RELEASE
    invoke VirtualFree, RotString, 00h, MEM_RELEASE
    mov eax, OrigStringIndex
    Ret
BWT_Transform endp

BWT_BubbleSort proc uses eax ebx ecx edx InputTable: DWORD, StringLength: DWORD
    local Swapped: DWORD
    local counter: DWORD

    ; Direct conversion of the BubbleSort I wrote for my Armadillo script LOL
    BS_OuterLoop:
        mov ebx, InputTable
        mov ecx, StringLength
        dec ecx
        mov Swapped, 0
        mov counter, 0
        BS_InnerLoop:
            mov eax, dword ptr ds:[ebx]
            mov edx, dword ptr ds:[ebx+4]
            invoke BWT_StrCmp, eax, edx, StringLength
            .if eax == 1
                mov eax, dword ptr ds:[ebx]
                mov dword ptr ds:[ebx], edx
                mov dword ptr ds:[ebx+4], eax
                mov Swapped, 1
            .endif
            add ebx, 4
            inc counter
        cmp counter, ecx
        jb BS_InnerLoop
    cmp Swapped, 1
    je BS_OuterLoop
    Ret
BWT_BubbleSort endp

BWT_StrCmp proc uses ebx ecx edx String1: DWORD, String2: DWORD, StringLength: DWORD
    local counter: DWORD

    mov ebx, String1
    mov ecx, StringLength
    mov edx, String2
    mov counter, 0
    .while counter < ecx
        movzx eax, byte ptr ds:[ebx]

        ; String1 > String2 -> EAX =  1
        ; String1 < String2 -> EAX = -1
        ; String1 = String2 -> EAX =  0
        .if al > byte ptr ds:[edx]
            mov eax, 1
            jmp @f
        .elseif al < byte ptr ds:[edx]
            mov eax, -1
            jmp @f
        .else
            xor eax, eax
        .endif
        inc counter
        inc ebx
        inc edx
    .endw

    @@:
    Ret
BWT_StrCmp endp
