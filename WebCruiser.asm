; Tested on Janusec WebCruiser 3.5.5

.code
Gen_WebCruiser proc in_username: DWORD, out_serial: DWORD
    invoke SHA512Init
    invoke StrLen, in_username
    invoke SHA512Update, in_username, eax
    invoke SHA512Final
    invoke HexEncode, eax, SHA512_DIGESTSIZE, offset WebCrui_TmpStr
    xor ecx, ecx
    xor edx, edx
    .while ecx <= SHA512_DIGESTSIZE*2
        movzx eax, byte ptr ds:[WebCrui_TmpStr+ecx]
        mov byte ptr ds:[WebCrui_FinHash+edx], al
        add ecx, 8
        inc edx
    .endw
    Invoke HexDecode, offset WebCrui_FinHash, offset WebCrui_TmpStr
    invoke HexDecode, offset WebCrui_MagicVa, offset WebCrui_TmpStr+8
    mov edx, dword ptr ds:[WebCrui_TmpStr]
    mov eax, dword ptr ds:[WebCrui_TmpStr+4]
    mov ecx, dword ptr ds:[WebCrui_TmpStr+8]
    mov ebx, dword ptr ds:[WebCrui_TmpStr+12]
    bswap eax
    bswap ebx
    bswap ecx
    bswap edx
    add eax, ebx
    adc edx, ecx
    mov ecx, edx
    shr edx, 16
    and ecx, 0FFFFh
    mov ebx, eax
    shr eax, 16
    and ebx, 0FFFFh
    invoke wsprintf, out_serial, offset WebCrui_SerForm, edx, ecx, eax, ebx
    Ret
Gen_WebCruiser endp
