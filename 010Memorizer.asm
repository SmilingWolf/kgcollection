; Tested on SweetScape's 010 Memorizer 1.1
OlOMemorizer3Bytes proto usersnumber: DWORD, version:DWORD, crc: DWORD
OlOMemorizerCRC proto in_username: DWORD, namelen: DWORD, version: DWORD, usersnumber: DWORD

.code
Gen_010Memorizer proc in_username: DWORD, out_serial: DWORD
    local namelen: DWORD
    local usersnumber: DWORD
    local version: DWORD
    local crc: DWORD
    local primo: DWORD
    local secondo: DWORD
    local terzo: DWORD
    local quarto: DWORD

    invoke StrLen, in_username
    mov namelen, eax
    mov usersnumber, 03E8h  ; 03E8h -> Site license
    invoke random, 0FFh     ; KeyVersion
    inc eax                 ; we make sure we pick a version number between 1 and 255
    mov version, eax
    invoke OlOMemorizerCRC, in_username, namelen, version, usersnumber
    bswap eax
    mov crc, eax

    invoke OlOMemorizer3Bytes, usersnumber, version, crc
    ror eax, 8
    movzx ebx, ax
    mov primo, ebx
    shr eax, 16
    add eax, 5Ah
    mov secondo, eax
    mov eax, crc
    shr eax, 16
    mov terzo, eax
    mov eax, crc
    and eax, 0FFFFh
    mov quarto, eax
    invoke wsprintf, out_serial, offset OlO_SerialFormat, primo, secondo, terzo, quarto
    Ret
Gen_010Memorizer endp

OlOMemorizer3Bytes proc usersnumber: DWORD, version:DWORD, crc: DWORD
    mov eax, usersnumber
    mov ebx, crc

    ;uno e due          ; users
    imul ecx, eax, 0Dh
    xor ecx, 7531h
    sub ecx, 0B3FAh
    xor ecx, 8E92h
    and ecx, 0FFFFh

    mov edx, version    ; program version
    xor edx, 0C9h
    sub edx, 0C1h
    xor edx, 0A7h
    and edx, 0FFh

    mov eax, crc
    shr eax, 8
    xor ecx, eax
    shr eax, 16
    xor edx, eax
    mov eax, edx
    shl eax, 16
    mov ax, cx
    Ret
OlOMemorizer3Bytes endp

OlOMemorizerCRC proc in_username: DWORD, namelen: DWORD, version: DWORD, usersnumber: DWORD
    local crc
    local counter
    local nameletter

    xor eax, eax
    xor ecx, ecx
    xor edx, edx
    mov crc, 00h
    mov counter, 00h

    .while eax < namelen
        mov eax, in_username
        mov ecx, counter
        movzx eax, byte ptr ds:[eax+ecx]
        mov nameletter, eax
        mov edx, dword ptr ds:[eax*4+OlOMemTbl]
        add edx, crc
        mov ecx, nameletter
        add ecx, 0Dh
        and ecx, 0FFh
        xor edx, dword ptr ds:[ecx*4+OlOMemTbl]
        mov eax, nameletter
        add eax, 02Fh
        and eax, 0FFh
        imul edx, dword ptr ds:[eax*4+OlOMemTbl]
        imul ecx, counter, 13h
        and ecx, 0FFh
        add edx, dword ptr ds:[ecx*4+OlOMemTbl]
        mov eax, counter
        lea eax, dword ptr ds:[eax+eax*8]
        mov ecx, version
        mov ebx, ecx
        shl ecx, 4
        add ecx, ebx
        add eax, ecx
        and eax, 0FFh
        add edx, dword ptr ds:[eax*4+OlOMemTbl]
        imul eax, counter, 0Dh
        mov ecx, usersnumber
        mov ebx, ecx
        shl ecx, 4
        sub ecx, ebx
        add eax, ecx
        and eax, 0FFh
        add edx, dword ptr ds:[eax*4+OlOMemTbl]
        mov crc, edx
        inc counter
        mov eax, counter
    .endw
    mov eax, crc
    Ret
OlOMemorizerCRC endp
