.code
Gen_EvOlUtIoNCM1 proc in_username: DWORD, out_serial: DWORD
    xor ecx, ecx
    .while ecx <= 12
        mov eax, in_username
        mov eax, [eax]
        mov ebx, offset ECM1_BaseString
        mov ebx, [ebx+ecx]
        sub ebx, 10101010h
        xor eax, ebx
        mov edx, offset ECM1_SerialHex
        mov [edx+ecx], eax
        add ecx, 4
    .endw

    xor ecx,ecx
    .while ecx <= 14
        mov eax, offset ECM1_SerialHex
        movzx eax, word ptr [eax+ecx]
        bswap eax
        shr eax, 10h
        mov ebx, offset ECM1_SerialHex
        mov word ptr [ebx+ecx], ax
        add ecx, 2
    .endw

    xor eax, eax
    mov ebx, offset ECM1_SerialHex
    mov ecx, out_serial
    .while eax <= 7
        invoke HexEncode, ebx, 2, ecx
        add ebx, 2
        add ecx, 5
        inc eax
    .endw

    mov ecx, 4
    .while ecx <= 34
        mov ebx, out_serial
        mov byte ptr [ebx+ecx], 2Dh
        add ecx, 5
    .endw
    Ret
Gen_EvOlUtIoNCM1 endp
