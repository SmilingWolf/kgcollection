; Tested on Mayura Software Mayura Draw 4.5

.code
Gen_MayuraDraw proc in_username: DWORD, out_serial: DWORD
    local NameLen

    ; Generazione dei primi 15 caratteri. Li generiamo a caso e poi ci infiliamo i caratteri buoni in seguito
    xor ecx, ecx
    mov ebx, offset MD_SerialFormat
    .while ecx < 0Fh    ; Genera i primi 15 byte a caso, il checksum lo calcoliamo dopo
        invoke random, 0Ah
        add eax, 30h
        mov byte ptr ds:[ebx+ecx], al
        inc ecx
    .endw

    ; Generazione del carattere per passare il version check. Primo e quarto carattere
    mov ebx, offset MD_GoodDigits
    invoke random, 06h
    movzx eax, byte ptr ds:[ebx+eax]
    mov ebx, offset MD_SerialFormat
    mov byte ptr ds:[ebx], al
    invoke random, 05h
    add eax, 35h
    mov byte ptr ds:[ebx+03h], al

    ; Generazione dei caratteri per il checksum del nome. Nono, undicesimo e dodicesimo carattere
    invoke szLower, in_username
    invoke StrLen, in_username
    mov NameLen, eax
    xor ecx, ecx
    xor edx, edx
    mov ebx, in_username
    .while ecx < NameLen
        movzx eax, byte ptr ds:[ebx+ecx]
        .if eax >= 61h && eax <= 7Ah
            add edx, eax
        .endif
        inc ecx
    .endw
    mov eax, edx
    cdq
    mov ecx, 3E8h
    div ecx
    add edx, 30h
    mov eax, edx
    .while eax > 39h
        sub eax, 0Ah
    .endw
    mov ebx, offset MD_SerialFormat
    mov byte ptr ds:[ebx+0Bh], al
    sub edx, eax
    mov eax, edx
    cdq
    mov ecx, 64h
    div ecx
    add eax, 30h
    mov byte ptr ds:[ebx+08h], al
    mov eax, edx
    cdq
    mov ecx, 0Ah
    div ecx
    add eax, 30h
    mov byte ptr ds:[ebx+0Ah], al

    ; Generazione dei caratteri per il check nascosto. Quinto, decimo e quindicesimo carattere
    mov ebx, offset MD_SerialFormat
    invoke random, 07h
    add eax, 30h
    mov byte ptr ds:[ebx+04h], al
    sub eax, 2Dh    ; Magic Value del check nascosto. Guarda gli appunti per l'equazione
    cdq
    mov ecx, 02h
    div ecx
    mov ecx, 35h
    add ecx, eax
    mov byte ptr ds:[ebx+09h], cl
    mov ecx, 35h
    sub ecx, eax
    sub ecx, edx
    mov byte ptr ds:[ebx+0Eh], cl

    ;Generazione del checksum del seriale. Ultimi 3 caratteri
    xor ecx, ecx
    xor edx, edx
    mov ebx, offset MD_SerialFormat
    .while ecx < 0Fh
        movzx eax, byte ptr ds:[ebx+ecx]
        sub eax, 30h
        add edx, eax
        inc ecx
    .endw
    add edx, 91h
    invoke wsprintf, out_serial, offset MD_SerialFormat, edx
    Ret
Gen_MayuraDraw endp
