bpx_BM1_CalcHash proto pUserName: DWORD, pSerial: DWORD
bpx_BM1_EAXCalc proto HashDWORD: DWORD

.code
Gen_bpx_BustMe1 proc in_username: DWORD, out_serial: DWORD
    local HashDWORD: DWORD

    xor ecx, ecx
    mov ebx, out_serial
    .while ecx < 04h
        invoke random, 9h
        add eax, 30h
        mov byte ptr ds:[ebx+ecx], al
        inc ecx
    .endw
    mov dword ptr ds:[ebx+4], 3747392Dh ; "-9G7"
    mov byte ptr ds:[ebx+8], 2Dh        ; "-"
    invoke bpx_BM1_CalcHash, in_username, out_serial
    and eax, 0FFFF0000h
    xor eax, 0A0000000h
    bswap eax
    mov HashDWORD, eax
    mov eax, out_serial
    add eax, 9
    invoke HexEncode, addr HashDWORD, 2, eax
    mov eax, out_serial
    add eax, 0Dh
    mov dword ptr ds:[eax], 32334333h   ; "3C32"
    Ret
Gen_bpx_BustMe1 endp

bpx_BM1_CalcHash proc pUserName: DWORD, pSerial: DWORD
    local counter1:DWORD
    local counter2:DWORD
    local HashDWORD: DWORD

    mov counter1, 0
    mov counter2, 96h
    mov HashDWORD, 0

    .while counter2 != 0
        mov eax, counter1
        add eax, pUserName
        .while byte ptr ds:[eax] != 0
            movsx eax, byte ptr [eax]
            add HashDWORD, eax
            invoke bpx_BM1_EAXCalc, HashDWORD
            mov HashDWORD, eax
            mov eax, pSerial
            movsx eax, byte ptr ds:[eax+2]
            xor HashDWORD, eax
            dec HashDWORD
            xor HashDWORD, 88888888h
            rol HashDWORD, 3
            mov eax, pSerial
            movsx eax, byte ptr ds:[eax+1]
            sub HashDWORD, eax
            mov eax, HashDWORD
            add HashDWORD, eax
            ror HashDWORD, 1
            invoke bpx_BM1_EAXCalc, HashDWORD
            mov HashDWORD, eax
            sub HashDWORD, 71855504h
            shl HashDWORD, 1
            mov eax, pSerial
            movsx eax, byte ptr ds:[eax]
            sub HashDWORD, eax
            xor HashDWORD, 0FFFFECA6h
            shr HashDWORD, 1
            invoke bpx_BM1_EAXCalc, HashDWORD
            mov HashDWORD, eax
            mov eax, counter1
            add eax, pUserName
            dec byte ptr [eax]
            mov ecx, counter1
            add ecx, pUserName
            mov edx, counter1
            add edx, pUserName
            mov eax, pSerial
            movzx eax, byte ptr ds:[eax+3]
            xor al, byte ptr [edx]
            mov byte ptr [ecx], al
            inc counter1
            mov eax, counter1
            add eax, pUserName
        .endw
        mov counter1, 0
        inc HashDWORD
        dec counter2
    .endw

    mov eax, HashDWORD
    Ret
bpx_BM1_CalcHash endp

bpx_BM1_EAXCalc proc HashDWORD: DWORD
    mov eax, HashDWORD
    xor ah, al
    xor ax, 1432h
    ror eax, 8
    bswap eax
    neg eax
    xor eax, 67FAE12Ch
    bswap eax
    Ret
bpx_BM1_EAXCalc endp