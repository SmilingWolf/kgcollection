; Apparently this was meant as a joke towards all the people going crazy for RSA
; Turns out I have no sense of humor lol
; N was factorized in about 3h using state of the art algos, implementations,
; software (YAFU/GGNFS/MSieve) and not-so-state-of-the-art hardware
; (a laptop with a 2.2GHz quad core Intel CPU)

.code
Gen_bartNinjaCrackMe proc in_username: DWORD, out_serial: DWORD
    local ptMD5Hash: DWORD

    invoke MD5Init
    invoke szUpper, in_username
    invoke StrLen, in_username
    invoke MD5Update, in_username, eax
    invoke MD5Final
    mov ptMD5Hash, eax
    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax
    invoke bnFromHex, offset bartNCM_D, BigD
    invoke bnFromHex, offset bartNCM_N, BigN
    invoke bnFromBytesEx, ptMD5Hash, MD5_DIGESTSIZE, BigC, FALSE
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    invoke bnToHex, BigM, out_serial
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish
    Ret
Gen_bartNinjaCrackMe endp
