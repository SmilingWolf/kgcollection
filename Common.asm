.code
random proc uses ecx edx Base:DWORD ; Park Miller random number algorithm
    mov eax, dword ptr [Seed]       ; from M32lib/nrand.asm
    xor edx, edx
    mov ecx, 127773
    div ecx
    mov ecx, eax
    mov eax, 16807
    mul edx
    mov edx, ecx
    mov ecx, eax
    mov eax, 2836
    mul edx
    sub ecx, eax
    xor edx, edx
    mov eax, ecx
    mov dword ptr [Seed], ecx
    div Base
    mov eax, edx
    Ret
random endp

; MSVC rand() as found in msvcrt.dll
msvc_random proc uses edx
    imul eax, dword ptr [Seed], 343FDh
    add eax, 269EC3h
    mov dword ptr [Seed], eax
    shr eax, 10h
    and eax, 7FFFh
    Ret
msvc_random endp

HexEncode proc uses edi esi ecx ebx eax pBuff: DWORD,dwLen: DWORD,pOutBuff: DWORD
    mov ebx,dwLen
    mov edi,pOutBuff
    .if ebx != 0
        mov esi,pBuff
        .while ebx != 0
            movzx eax,byte ptr ds:[esi]
            mov ecx,eax
            add edi,2
            shr ecx,4
            and eax,0Fh
            and ecx,0Fh
            cmp eax,0Ah
            sbb edx,edx
            adc eax,0
            lea eax,dword ptr ds:[eax+edx*8+37h]
            cmp ecx,0Ah
            sbb edx,edx
            adc ecx,0
            shl eax,8
            lea ecx,dword ptr ds:[ecx+edx*8+37h]
            or eax,ecx
            inc esi
            mov word ptr ds:[edi-2],ax
            dec ebx
        .endw
    .endif
    mov eax,edi
    mov byte ptr ds:[edi],0
    sub eax,dword ptr ss:[ebp+10h]
    Ret
HexEncode endp

; http://www.asmcommunity.net/forums/topic/?id=4628#post-32472
CalcCRC32 proc uses ebx esi edi buf:DWORD, len:DWORD
mov esi, buf
mov edi, offset CRC32Table
mov edx, len
shr edx, 1
or ecx, -1
xor eax, eax
@@:
mov al, [esi]
xor al, cl
shr ecx, 8
mov ebx, [edi+4*eax]
xor ecx, ebx

mov al, [esi+1]
xor al, cl
shr ecx, 8
mov ebx, [edi+4*eax]
add esi,2
xor ecx, ebx

dec edx
jnz @B

test len, 1
jz @F
mov al, [esi]
xor al, cl
inc esi
shr ecx, 8
mov ebx, [edi+4*eax]
xor ecx, ebx
@@:

mov eax, ecx
not eax
ret
CalcCRC32 endp

CalcCRC32b proc uses ebx esi edi buf:DWORD, len:DWORD
    mov esi, buf
    mov ecx, len
    add esi, ecx
    neg ecx
    xor eax, eax
    xor eax, 0FFFFFFFFh
    xor edx, edx
    .while ecx > 0
        mov dl, byte ptr ds:[ecx+esi]
        mov ebx, eax
        shr ebx, 24
        xor edx, ebx
        mov ebx, dword ptr ds:[CRC32bTable+edx*4]
        shl eax, 8
        xor eax, ebx
        inc ecx
    .endw
    xor eax, 0FFFFFFFFh
    bswap eax
    Ret
CalcCRC32b endp

XTEAEncypher proc uses ebp edi esi ebx num_rounds: DWORD, v: DWORD, k: DWORD
    local sum: DWORD

    mov eax, num_rounds
    mov esi, v
    mov edi, dword ptr ds:[esi]
    mov esi, dword ptr ds:[esi+4]
    .if eax != 00h
        imul eax, eax, 9E3779B9h
        xor ebx, ebx
        mov sum, eax
        mov esi, esi
        lea edi, dword ptr ds:[edi]
        .repeat
            push ebp
            mov ebp, k
            mov eax, ebx
            mov ecx, esi
            and eax, 3
            shr ecx, 5
            mov edx, dword ptr ss:[ebp+eax*4]
            mov eax, esi
            shl eax, 4
            xor eax, ecx
            add eax, esi
            add edx, ebx
            sub ebx, 61C88647h
            xor edx, eax
            add edi, edx
            mov edx, edi
            mov eax, edi
            shr edx, 5
            shl eax, 4
            xor eax, edx
            mov edx, ebx
            shr edx, 0Bh
            add eax, edi
            and edx, 3
            mov ecx, dword ptr ss:[ebp+edx*4]
            add ecx, ebx
            xor eax, ecx
            add esi, eax
            pop ebp
        .until ebx == sum
    .endif
    mov eax, v
    mov dword ptr ds:[eax], edi
    mov dword ptr ds:[eax+4], esi
    Ret
XTEAEncypher endp
