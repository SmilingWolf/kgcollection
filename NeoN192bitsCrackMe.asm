.code
Gen_NeoN192bitsCrackMe proc in_username: DWORD, out_serial: DWORD
    invoke RtlZeroMemory, offset NeoN192bits_TSt, sizeof NeoN192bits_TSt

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax
    invoke bnFromHex, offset NeoN192bits_D, BigD
    invoke bnFromHex, offset NeoN192bits_N, BigN
    invoke StrLen, in_username
    invoke bnFromBytes, in_username, eax, BigC, FALSE
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    invoke bnToHex, BigM, offset NeoN192bits_TSt
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish
    invoke StrLen, offset NeoN192bits_TSt
    mov ecx, eax
    mov esi, offset NeoN192bits_TSt - 1
    mov edi, out_serial
    .while ecx
        movzx eax, byte ptr ds:[esi+ecx]
        shl eax, 8
        dec ecx
        mov al, byte ptr ds:[esi+ecx]
        dec ecx
        mov word ptr ds:[edi], ax
        add edi, 2
    .endw
    Ret
Gen_NeoN192bitsCrackMe endp
