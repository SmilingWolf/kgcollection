.code
Gen_OJFunKGM2 proc in_username: DWORD, out_serial: DWORD
    invoke SHA1Init
    invoke StrLen, in_username
    invoke SHA1Update, in_username, eax
    invoke SHA1Final
    mov ecx, 0Dh
    mov esi, eax
    mov edi, offset OJKM_TempString
    rep movsb
    mov eax, out_serial
    mov dword ptr ds:[eax], "51JO"
    mov dword ptr ds:[eax+4], "-"

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax
    invoke bnFromHex, offset OJKM_D, BigD
    invoke bnFromBytesEx, offset OJKM_TempString, 0Dh, BigC, FALSE
    invoke bnFromHex, offset OJKM_N, BigN
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    mov eax, out_serial
    add eax, 5
    invoke bnToHex, BigM, eax
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish
    Ret
Gen_OJFunKGM2 endp
