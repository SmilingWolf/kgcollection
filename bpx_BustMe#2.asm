.code
Gen_bpx_BustMe2 proc in_username: DWORD, out_serial: DWORD
    local crc1: DWORD
    local crc2: DWORD
    local namelen: DWORD
    local destlen: DWORD

    invoke StrLen, in_username
    mov namelen, eax
    invoke CalcCRC32, in_username, namelen
    bswap eax
    mov crc1, eax
    invoke HexEncode, addr crc1, 4, offset bpKM_CRC
    invoke CalcCRC32, offset bpKM_CRC, 8
    bswap eax
    mov crc2, eax
    invoke HexEncode, addr crc2, 4, offset bpKM_CRC+8
    mov destlen, sizeof bpKM_CompBuffer
    invoke compress, offset bpKM_CompBuffer, addr destlen, offset bpKM_CRC, 16
    invoke HexEncode, offset bpKM_CompBuffer, destlen, out_serial
    Ret
Gen_bpx_BustMe2 endp
