; Tested on Ardamax Keylogger 4.7

; Sooooo! This was one of the most interesting things I reversed in a while :D
; What happens here? Ardamax author generated a stream of 5000 bytes using
; something like
; srand(unknownSeed);
; char plaintextsStream[5000];
; for (int i = 0; i < 5000; i++) {
;   plaintextsStream[i] = rand() & 0xFF;
; }
; There was no way to know this, so I made a guess, ripped rand() from msvcrt.dll
; and just did some experiments. After some tinkering I was able to recover plaintextsStream
; by bruteforcing unknownSeed. Luckily the author gave away some plaintexts to test with
; when he included some blacklisted keys in the executable.
; Ardamax author then generated a table of of the first 5 bytes of the MD4 values
; each calculated using 5 bytes from the plaintextsStream using something like
; char MD4sTable[5000];
; for (int i = 0; i < 1000; i++) {
;   MD4(&plaintextsStream[i*5], 5, &MD4Store);
;   memcpy(&MD4sTable+(i*5), &MD4Store; 5);
; }
; Results: knowing the seed I can choose a random value RandomCounter between 0 and 1000,
; seed the PRNG with unknownSeed (now known), call it RandomCounter * 5 times in order to
; discard the bytes I don't need, then generate the plaintext corresponding to the MD4 stored
; at offset MD4sTable + (RandomCounter * 5) in the Ardamax Keylogger executable

ArdaKey_MD4Loop proto in_bytes: DWORD, out_address: DWORD

.code
Gen_ArdamaxKeylogger proc out_serial: DWORD
    local RandomCounter: DWORD

    invoke random, 1000
    mov RandomCounter, eax

    mov dword ptr ds:[Seed], 7DD9Ah
    mov ecx, RandomCounter
    imul ecx, ecx, 5
    .while ecx > 0
        invoke msvc_random
        dec ecx
    .endw
    .while ecx < 5
        invoke msvc_random
        mov byte ptr ds:[ArdaKey_FiBytes+ecx], al
        inc ecx
    .endw

    ; Let's restore a bit of randomness
    rdtsc
    xor dword ptr ds:[Seed], eax
    .repeat
        invoke random, 0FFFFFFFFh
        or eax, 600000h     ; Edition bits
        or eax, 800000h     ; Version bits (set lower bit)
        and eax, 0FEFFFFFFh ; Version bits (clear higher bit)
        mov dword ptr ds:[ArdaKey_Tmp2nd], eax
        invoke random, 0FFh
        mov byte ptr ds:[ArdaKey_Tmp2nd+4], al
        invoke ArdaKey_MD4Loop, offset ArdaKey_Tmp2nd, offset ArdaKey_TmpHash
        mov ecx, 1000
        mov eax, dword ptr ds:[ArdaKey_TmpHash]
        cdq
        idiv ecx
        test edx, edx
        jge @f
        neg edx
        @@:
    .until edx == RandomCounter

    mov esi, offset ArdaKey_Tmp2nd
    mov edi, offset ArdaKey_FiBytes+5
    mov ecx, 5
    rep movsb
    mov eax, dword ptr ds:[ArdaKey_FiBytes]
    xor dword ptr ds:[ArdaKey_FiBytes+5], eax
    mov al, byte ptr ds:[ArdaKey_FiBytes+4]
    xor byte ptr ds:[ArdaKey_FiBytes+9], al

    mov esi, offset ArdaKey_FiBytes
    mov edi, offset ArdaKey_AlphIdx
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    .while edx < 8
        mov eax, dword ptr ds:[esi+edx]
        shr eax, cl
        and eax, 1Fh
        mov byte ptr ds:[edi+ebx], al
        inc ebx
        mov eax, dword ptr ds:[esi+edx]
        add ecx, 5
        shr eax, cl
        and eax, 1Fh
        mov byte ptr ds:[edi+ebx], al
        inc edx
        inc ebx
        sub ecx, 3
    .endw

    mov esi, offset ArdaKey_AlphIdx
    mov edi, out_serial
    xor ecx, ecx
    .while ecx < 16
        movzx eax, byte ptr ds:[esi+ecx]
        mov al, byte ptr ds:[ArdaKey_Alpha+eax]
        mov byte ptr ds:[edi+ecx], al
        inc ecx
    .endw

    Ret
Gen_ArdamaxKeylogger endp

ArdaKey_MD4Loop proc in_bytes: DWORD, out_address: DWORD
    local BaseMD4: DWORD
    local Counter: DWORD

    invoke MD4Init
    invoke MD4Update, in_bytes, 5
    invoke MD4Final
    mov BaseMD4, eax

    mov Counter, 0
    .while Counter < 499
        mov edi, out_address
        mov esi, BaseMD4
        mov ecx, 4
        rep movsd
        mov ecx, out_address
        mov BaseMD4, ecx
        mov edi, BaseMD4
        mov esi, 5
        mov ebx, in_bytes
        xor ecx, ecx
        .while ecx < 16
            mov eax, ecx
            cdq
            idiv esi
            mov al, byte ptr ds:[ebx+edx]
            xor byte ptr ds:[edi+ecx], al
            inc ecx
        .endw
        invoke MD4Init
        invoke MD4Update, out_address, 16
        invoke MD4Final
        mov BaseMD4, eax
        inc Counter
    .endw
    mov edi, out_address
    mov esi, BaseMD4
    mov ecx, 4
    rep movsd
    Ret
ArdaKey_MD4Loop endp
