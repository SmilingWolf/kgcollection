.data
Seed            db  "1337", 00h

; Keylogger Detector 1.36
KD_MagicString  db  5Fh, 74h, 20h, 3Ch, 28h, 36h, 3Dh, 6Bh, 6Ch, 2Fh, 31h, 37h, 67h, 78h, 2Ch, 3Eh, 00h

; Perfect Keylogger 1.68.2
PK_MagicString  db  5Fh, 72h, 20h, 3Ch, 28h, 29h, 3Ch, 31h, 2Dh, 5Ah, 32h, 5Bh, 6Ch, 35h, 2Ch, 5Eh, 00h

; Boss Invisible 1.02
BI_MagicString  db  71h, 49h, 24h, 0F0h, 0CDh, 38h, 39h, 29h, 26h, 45h, 0F3h, 62h, 2Ah, 0C8h, 67h, 7Bh, 00h

; RLPack v1.21
RL_SerialFormat db  "TheChant=%s", 0Dh, 0Ah, "ACurse=%s", 0Dh, 0Ah, "TheChicks=%X", 00h
RL_FileName     db  "license.reg", 00h

; 010 Editor 6.0.3
OlO_SerialFormat db "%.04X-%.04X-%.04X-%.04X", 00h

; Hamic 2.0.1
Ham_SerialFormat db "%.04X-%.04X-%.04X-%.04X", 00h

; Mayura Draw 4.5
MD_SerialFormat db  "000000000000000%d", 00h
MD_GoodDigits   db  "014589", 00h

; WinCHM Pro 5.11
WCHM_Alpha      db  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 00h

; WordToHelp 3.13
Word2Help_Alpha db  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 00h

; CHM to PDF converter 3.07
CHM2PDF_Alpha   db  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 00h

; CHM to DOC converter 3.07
CHM2DOC_Alpha   db  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 00h

; VMWare Workstation Pro 12.1.1
VMW_Alpha       db  "0123456789ACDEFGHJKLMNPQRTUVWXYZ", 00h
VMW_SHAConstStr db  "9811fc4e-e4f95a51-1b40c17c-ad0b652e-20218f07", 00h
VMW_SHAConstBlk db  7Dh, 0C7h, 89h, 0F9h, 69h, 5Dh, 71h, 90h, 0FAh, 84h, 22h, 0CAh, 0BFh, 1Dh, 0C0h, 0C0h, 88h, 42h, 37h, 0D5h, 70h, 80h, 2Eh, 83h, 0C7h, 92h, 0A3h, 59h, 0C6h, 95h, 24h, 0BFh
VMW_LetOrder    db  19h, 15h, 01h, 10h, 13h, 00h, 14h, 08h, 16h, 18h, 1Bh, 02h, 09h, 0Ah, 06h, 0Eh, 1Ah, 1Ch, 03h, 04h, 0Ch, 0Dh, 07h, 0Fh, 12h

; Folder Lock 7.6.0
FL_FixedString  db  "tEN.sErAwlockTfOsWeNFolder", 00h
FL_SerialFormat db  "%.08X%.08X%.08X%.08X%.08X", 00h

; Video Converter Ultimate 7.8.19
VidConU_Alpha   db  "0123456789ABCDEF", 00h
VidConU_Magic   db  31h, 78h, 01h, 6Ch, 03h, 73h, 05h, 66h, 07h, 76h, 09h, 64h, 0Bh, 6Fh, 0Dh, 6Fh, 0Fh, 76h, 11h, 72h
                db  13h, 65h, 15h, 75h, 17h, 74h, 19h, 6Dh, 1Bh, 74h, 1Dh, 37h, 1Fh, 69h, 02h, 69h, 04h, 6Fh, 06h, 74h
                db  08h, 69h, 0Ah, 65h, 0Ch, 63h, 0Eh, 6Eh, 10h, 65h, 12h, 74h, 14h, 72h, 16h, 6Ch, 18h, 69h, 1Ah, 61h
                db  1Ch, 65h, 1Eh, 30h, 30h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h
                db  00h, 00h, 00h, 00h, 00h, 78h, 69h, 6Ch, 69h, 73h, 6Fh, 66h, 74h, 76h, 69h, 64h, 65h, 6Fh, 63h, 6Fh
                db  6Eh, 76h, 65h, 72h, 74h, 65h, 72h, 75h, 6Ch, 74h, 69h, 6Dh, 61h, 74h, 65h, 37h
VidConU_SerForm db  "%.04X-%.04X-%.04X-%.04X", 00h

; DVD Ripper Ultimate SE 7.8.19
DVDRipU_Alpha   db  "0123456789ABCDEF", 00h
DVDRipU_Magic   db  31h, 78h, 01h, 6Ch, 03h, 73h, 05h, 66h, 07h, 64h, 09h, 64h, 0Bh, 69h, 0Dh, 70h, 0Fh, 72h, 11h, 6Ch
                db  13h, 69h, 15h, 61h, 17h, 65h, 19h, 69h, 02h, 69h, 04h, 6Fh, 06h, 74h, 08h, 76h, 0Ah, 72h, 0Ch, 70h
                db  0Eh, 65h, 10h, 75h, 12h, 74h, 14h, 6Dh, 16h, 74h, 18h, 37h, 1Ah, 30h, 30h, 00h, 00h, 00h, 00h, 00h
                db  00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 78h, 69h, 6Ch, 69h, 73h
                db  6Fh, 66h, 74h, 64h, 76h, 64h, 72h, 69h, 70h, 70h, 65h, 72h, 75h, 6Ch, 74h, 69h, 6Dh, 61h, 74h, 65h
                db  37h
DVDRipU_SerForm db  "%.04X-%.04X-%.04X-%.04X", 00h

; YouTube Video Converter 5.6.7
YTViCon_Alpha   db  "0123456789ABCDEF", 00h
YTViCon_Magic   db  31h, 78h, 01h, 6Ch, 03h, 73h, 05h, 66h, 07h, 79h, 09h, 75h, 0Bh, 75h, 0Dh, 65h, 0Fh, 69h, 11h, 65h
                db  13h, 63h, 15h, 6Eh, 17h, 65h, 19h, 74h, 1Bh, 72h, 1Dh, 69h, 02h, 69h, 04h, 6Fh, 06h, 74h, 08h, 6Fh
                db  0Ah, 74h, 0Ch, 62h, 0Eh, 76h, 10h, 64h, 12h, 6Fh, 14h, 6Fh, 16h, 76h, 18h, 72h, 1Ah, 65h, 1Ch, 35h
                db  1Eh, 30h, 30h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h
                db  00h, 00h, 00h, 78h, 69h, 6Ch, 69h, 73h, 6Fh, 66h, 74h, 79h, 6Fh, 75h, 74h, 75h, 62h, 65h, 76h, 69h
                db  64h, 65h, 6Fh, 63h, 6Fh, 6Eh, 76h, 65h, 72h, 74h, 65h, 72h, 35h
YTViCon_SerForm db  "%.04X-%.04X-%.04X-%.04X", 00h

; WebCruiser 3.5.5
WebCrui_MagicVa db  "7FA1447820DA2C6C", 00h
WebCrui_SerForm db  "%.02X-%.02X-%.02X-%.02X", 00h

; WinImage 9.00
WinImage_MgcVal dd  10051981h, 2061997h, 16062004h, 13062004h, 24112005h
WinImage_SerFor db  "%.08X", 00h

; SmartVersion 3.60
SmartVer_MgcVal dd  31121999h, 2062000h, 21032004h, 28032004h
SmartVer_SerFor db  "%.08X", 00h

; ZIP Password Refixer 3.1.1
ZPassRef_SerFor db  "ISUMSOFT-ZIP-REFIXER%08X-SW", 00h

; Ardamax Keylogger 4.7
ArdaKey_Alpha   db "0123456789ABCDEFGHIJKLMNOPQRSTUV", 00h

; Eggi's ArTeam Crackme #1
Ar_Alpha        db  "DEFGHIJKLMNOPQRSTUVWXYZ", 00h

; EvOlUtIoN's CM1
ECM1_BaseString db  "Registered to:  ", 00h

; Office Jesus' Fun KeygenMe #2
OJKM_D          db  "20984E8738CAFEA6B8DD6F5F7D997EB9", 00h
OJKM_N          db  "44494F47454E45534F4653494E4F5045", 00h

; bart/xt's Ninja CrackMe
bartNCM_D       db  "A5389CB9DD5609F7130CCE6E4FE5F057FA636BA68B5B14672C7BD4DC26666F9A6BE23E30E2F12A01D", 00h
bartNCM_N       db  "AB185AE9243F57C7428B7CE76B62A5E1321CA3A3F966659A8368BD3879241F35407599BDC49EA31A9", 00h

; bart/xt's PRNG CrackMe
PRNGCM_NoPRNG   db  "The correct PRNG seed could not be found", 00h

; Thigo's Little KeygenMe
ThKM_SrllFormat db  "Thigo ufound %s", 00h
ThKM_D          db  "78076BDC2928F59C2ABB141DE14F6F73B43D1EEADBAEC09D09E3F1A69E11", 00h
ThKM_N          db  "B9B9BCDA766CC754AF5A3AA6A4302E0C62B455B6977488F9CDAD8F2F9479", 00h
ThKM_FileName   db  "key.dat", 00h

; TMG's Official KeygenMe [No.2]
TMG2_D          db  "C6546E0C11ACCE2543DD1150C4CE7A05A4C8FA3D", 00h
TMG2_N          db  "D2E9BF9B3D258E479D8CC23C7A33E1F8EBB3ADB1", 00h

; TMG's Official KeygenMe [No.3]
TMG3_FirstPow   db  "03", 00h
TMG3_MMod       db  "37A218F214C32D79", 00h
TMG3_P          db  "C9D94F46D0984F42", 00h
TMG3_G          db  "4B45042B684BCBD1", 00h
TMG3_X          db  "6C18DA28FDD8FEF1", 00h

; Crudd's Kryptonite KeygenMe
CruKry_Key      db  "ReverseEngineer!", 00h

; Cronos' CrackMe 4
CroCM4_FileName db  "iss.rba", 00h

; HappyTown's CrackMe_0031
HTCM31_SFormat  db "%.08X-%.08X-%.08X-%.08X", 00h

; HappyTown's CrackMe_0034
HTCM34_Magic0   db  "c2RmcXdlcjQxNTE1MTUxamwzMjE1QCQhMzQyZmE=", 00h
HTCM34_Magic1   db  "MzQ1MzQ1MjM0JkBTREFGYXNkZjIzMTMyMTMX124=", 00h
HTCM34_N        db  "886A71F603197C430E9C473BB6991BE95B45AB519F57ADB9", 00h
HTCM34_D        db  "7C3E3E86EAB1C67C288D6B4E01CD7C11196B29622F848985", 00h

; ORiON's Official KeygenMe - Hard
OROffKGMH_NoJoy db  "Can't generate a valid serial for this username", 00h
OROffKGMH_N     db  "E8A1276B2AB94FEA3BF1410603600B43E2010A99", 00h
OROffKGMH_D     db  "C54FA32297BB5458257BEEA22442F8A320963FAD", 00h

; Cyclops' Chakravyuha KeygenMe
CyChakraKGM_SF  db  "%.08X-%.08X-%.08X-%.08X", 00h

; MagiK's Anti - OllyDbg 1.0
MagAODbg_wsform db  "%ld", 00h

; RED Crew's Official KeygenMe #1
REDCKGM_P       db  "FDABE5F65AB046B1DC65A797EB3348EF", 00h
REDCKGM_G       db  "9CC56771014FD88F9B2EC5B0ADE75183", 00h
REDCKGM_X       db  "58D6DC80E9A83B66E880864FAF7A17E5", 00h
REDCKGM_NR      db  "A165467291A36424EFF4DF9503317711", 00h
REDCKGM_DR      db  "9A5DFE6A7867D9D07A32AECC462ED1FD", 00h
REDCKGM_NS      db  "B8393E870DE280A03A9D978C19960299", 00h
REDCKGM_DS      db  "7E47DF30BCB7D2EAD8D00FD9CF1CEC71", 00h
REDCKGM_AlphaFr db  "0123456789ABCDEF-", 00h
REDCKGM_AlphaTo db  "hZGm4Lr9QwfXEk2Jd", 00h

; x15or's Broken Lands KeygenMe
x15BroLan_02wsp db  "%02x", 00h
x15BroLan_08wsp db  "%08X%08X", 00h
x15BroLan_Key   db  42h, 52h, 4Fh, 4Bh, 45h, 4Eh, 2Dh, 4Ch, 41h, 4Eh, 44h, 53h, 00h, 00h, 00h, 00h
x15BroLan_N     db  "7CB2EE4EA00A89C1A1BA69BEFFFCDFC0F5C6475A9694FD61309B1", 00h
x15BroLan_D     db  "621AC3B1C7E782897C1B94A657A6BCE9BE9338991366B710C4B21", 00h

; NeoN's 192bits CrackMe
NeoN192bits_N   db  "B0C83F8F63BFDDC3C4F8EF72D27029ED6EEC41A71892BD69", 00h
NeoN192bits_D   db  "384F2087C0E31D753D2979C3552B31E772147015771E07ED", 00h

; Xylitol's BarbecueMe
XyliBBQMe_Hexws db  "%.X", 00h
XyliBBQMe_BBQSt db  "-OMGWTFBBQ", 00h

; HMX0101's Alive KeygenMe
HMXAlive_P      db  "E50D80BB9284DF3F23D3", 00h
HMXAlive_G      db  "9F76B9617C4B04782B89", 00h
HMXAlive_X      db  "61A2A8E3C12EAF35A63D", 00h
HMXAlive_3Chars db  "ZZ5", 00h
HMXAlive_Decwsp db  "%02d", 00h

; lostit KeygenMe 1
lostKGM1_NoJoy  db  "Can't generate a valid serial for this username", 00h

.data?
; RLPack v1.21
RL_CattedString dd  50 dup(?)

; VMWare Workstation Pro v12.1.1
VMW_RandomMsg   db  11 dup(?)
VMW_MsgChecksum db  05 dup(?)
VMW_Serial5Bit  db  16 dup(?)
VMW_SerialIndxs db  25 dup(?)
VMW_SrScattered db  25 dup(?)
VMW_TempString  db  16 dup(?)

; Folder Lock 7.6.0
FL_HashState    db 101 dup(?)

; Video Converter Ultimate 7.8.19
VidConU_TmpStr  db   8 dup(?)

; DVD Ripper Ultimate SE 7.8.19
DVDRipU_TmpStr  db   8 dup(?)

; YouTube Video Converter 5.6.7
YTViCon_TmpStr  db   8 dup(?)

; WebCruiser 3.5.5
WebCrui_TmpStr  db 130 dup(?)
WebCrui_FinHash db  18 dup(?)

; WinImage 9.00
WinImage_TmpStr db  10 dup(?)

; SmartVersion 3.60
SmartVer_TmpStr db  10 dup(?)

; Ardamax Keylogger 4.7
ArdaKey_TmpHash db  16 dup(?)
ArdaKey_Tmp2nd  db   5 dup(?)
ArdaKey_FiBytes db  10 dup(?)
ArdaKey_AlphIdx db  16 dup(?)

; EvOlUtIoN's CM1
ECM1_SerialHex  dd  50 dup(?)

; jj89XD's Crackme
jjCM_TempString dd  50 dup(?)

; Woot332's KeygenMe
WKGM_CRC1       dd  ?
WKGM_CRC2       dd  ?

; mm10121991's UnpackMe+KeygenMe
mmKM_TempString dd  50 dup(?)

; bpx_'s BustMe #2
bpKM_CRC        dd   4 dup(?)
bpKM_CompBuffer db  30 dup(?)

; Misc BigNums
BigTmp          dd  ?  ;conterra' vari BigNum temporanei
BigC            dd  ?  ;conterra' il cyphertext (C)
BigD            dd  ?  ;conterra' la private key (D)
BigN            dd  ?  ;conterra' il modulo (N)
BigM            dd  ?  ;conterra' il messaggio decifrato/plaintext (M)

BigH            dd  ?
BigMMod         dd  ?
BigP            dd  ?
BigK            dd  ?
BigG            dd  ?
BigR            dd  ?
BigS            dd  ?
BigX            dd  ?

; Office Jesus' Fun KeygenMe #2
OJKM_TempString dd  50 dup(?)

; bart/xt's PRNG CrackMe
PRNGCM_TmString dd  50 dup(?)

; Thigo's Little KeygenMe
ThKM_TempString dd  50 dup(?)

; TMG's Official KeygenMe [No.2]
TMG2_TempString dd  50 dup(?)

; TMG's Official KeygenMe [No.3]
TMG3_TempString dd  50 dup(?)

; HappyTown's CrackMe_0010
HTCM10_AsciiCRC db  10 dup(?)

; HappyTown's CrackMe_0034
HTCM34_TigerH   db  24 dup(?)
HTCM34_HexM     dd  50 dup(?)
HTCM34_TempStr  dd  50 dup(?)

; ORiON's Official KeygenMe - Hard
OROffKGMH_Hash  db  20 dup(?)

; Cyclops's Chakravyuha KeygenMe
CyChakraKGM_Enc db  16 dup(?)

; MagiK's Anti - OllyDbg 1.0
MagAODbg_TempSt dd  50 dup(?)

; RED Crew's Official KeygenMe #1
REDCKGM_NSumStr db   9 dup(?)
REDCKGM_TempStr dd  50 dup(?)

; x15or's Broken Lands KeygenMe
x15BroLan_TStr  dd  50 dup(?)

; NeoN's 192bits CrackMe
NeoN192bits_TSt dd  50 dup(?)

; Xylitol's BarbecueMe
XyliBBQMe_TStr  dd  50 dup(?)
XyliBBQMe_BFisS dd  50 dup(?)

; HMX0101's Alive KeygenMe
HMXAlive_TmpStr dd  50 dup(?)

; lostit KeygenMe 1
lostKGM1_TmpUsr db 256 dup(?)
lostKGM1_Hash   db  16 dup(?)
lostKGM1_FiByte db  12 dup(?)
