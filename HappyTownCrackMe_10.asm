.code
Gen_HappyTownCrackMe_10 proc in_username: DWORD, out_serial: DWORD
    local crc: DWORD
    local ptMD5Hash: DWORD

    invoke StrLen, in_username
    invoke CRC32, in_username, eax, 00h
    bswap eax
    mov crc, eax
    invoke HexEncode, addr crc, 4, offset HTCM10_AsciiCRC
    invoke MD5Init
    invoke MD5Update, offset HTCM10_AsciiCRC, 8
    invoke MD5Final
    mov ptMD5Hash, eax

    ror word ptr ds:[eax+8], 4

    mov ebx, crc
    bswap ebx
    xor dword ptr ds:[eax], ebx

    mov ebx, dword ptr ds:[eax]
    mov ecx, dword ptr ds:[eax+4]
    shr ebx, 24
    shr ecx, 24
    shl dword ptr ds:[eax], 8
    shl dword ptr ds:[eax+4], 8
    or dword ptr ds:[eax], ecx
    or dword ptr ds:[eax+4], ebx

    ror byte ptr ds:[eax+4], 4
    ror byte ptr ds:[eax+5], 4
    ror word ptr ds:[eax+4], 8

    ror byte ptr ds:[eax+2], 4
    ror byte ptr ds:[eax+3], 4
    ror word ptr ds:[eax+2], 8

    xor dword ptr ds:[eax], 452821E6h
    xor dword ptr ds:[eax+4], 38D01377h
    xor dword ptr ds:[eax+8], 37D0D724h
    xor dword ptr ds:[eax+12], 34E90C6Ch

    mov ebx, dword ptr ds:[eax]
    bswap ebx
    mov dword ptr ds:[eax], ebx
    mov ebx, dword ptr ds:[eax+4]
    bswap ebx
    mov dword ptr ds:[eax+4], ebx
    mov ebx, dword ptr ds:[eax+8]
    bswap ebx
    mov dword ptr ds:[eax+8], ebx
    mov ebx, dword ptr ds:[eax+12]
    bswap ebx
    mov dword ptr ds:[eax+12], ebx

    invoke HexEncode, ptMD5Hash, MD5_DIGESTSIZE, out_serial
    Ret
Gen_HappyTownCrackMe_10 endp
