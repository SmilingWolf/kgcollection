; Tested on iSumsoft ZIP Password Refixer 3.1.1
; Just in case you're wondering, the SW at the end of the
; serial is there simply because of personal vanity :P

.code
Gen_ZIPPasswordRefixer proc out_serial: DWORD
    invoke random, 0FFFFFFFFh
    invoke wsprintf, out_serial, offset ZPassRef_SerFor, eax
    Ret
Gen_ZIPPasswordRefixer endp
