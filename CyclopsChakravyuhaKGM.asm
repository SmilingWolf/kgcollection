; This one was a nice little f*cker. A really entertaining one.
; A short VM (so short I could fully devirtualize it by hand)
; with variable length entries. Once devirtualized you get to see
; you're dealing with a short custom decryption algo that takes your
; serial in input and spits out the decrypted values, which must match
; the username. Mind that the block length is 16 bytes and if your
; username is shorter than that it gets padded with the first bytes
; of the block until it matches the blocklen.

ChakraEncrypt proto lpInBuffer: DWORD, lpOutBuffer: DWORD

.code
Gen_CyclopsChakravyuhaKGM proc in_username: DWORD, out_serial: DWORD
    invoke StrLen, in_username
    .if eax < 16
        mov ebx, eax
        sub eax, 16
        not eax
        inc eax
        mov ecx, eax
        mov esi, in_username
        mov edi, in_username
        add edi, ebx
        rep movsb
    .endif
    invoke ChakraEncrypt, in_username, offset CyChakraKGM_Enc
    mov eax, offset CyChakraKGM_Enc
    mov ebx, [eax]
    mov ecx, [eax+4]
    mov edx, [eax+8]
    mov eax, [eax+12]
    invoke wsprintf, out_serial, offset CyChakraKGM_SF, ebx, ecx, edx, eax
    Ret
Gen_CyclopsChakravyuhaKGM endp

ChakraEncrypt proc lpInBuffer: DWORD, lpOutBuffer: DWORD
    mov eax, lpInBuffer
    mov edx, [eax]      ; Parte 1 del blocco
    mov ebx, [eax+4]    ; Parte 2 del blocco
    mov esi, [eax+8]    ; Parte 3 del blocco
    mov edi, [eax+12]   ; Parte 4 del blocco

    bswap edx
    bswap ebx
    bswap esi
    bswap edi

    mov ecx, 00h
    .repeat
        xor edx, ebx
        xor edx, 12341234h
        rol edx, cl

        xor esi, edi
        xor esi, 12341234h
        ror esi, cl

        xor edi, edx
        xor edi, 12341234h
        rol edi, cl

        xor ebx, esi
        xor ebx, 12341234h
        ror ebx, cl

        xchg ebx, edi
        xchg esi, edx
        inc ecx
    .until ecx > 7

    mov eax, lpOutBuffer
    mov [eax], edx
    mov [eax+4], ebx
    mov [eax+8], esi
    mov [eax+12], edi
    Ret
ChakraEncrypt endp
