lostKGM1_StripChars proto in_username: DWORD, out_address: DWORD
lostKGM1_NameMD5Loop proto in_bytes: DWORD, out_address: DWORD

.code
Gen_lostitKGM_01 proc in_username: DWORD, out_serial: DWORD
    local RandomCounter: DWORD

    invoke RtlZeroMemory, offset lostKGM1_TmpUsr, sizeof lostKGM1_TmpUsr
    invoke lostKGM1_StripChars, in_username, offset lostKGM1_TmpUsr
    invoke StrLen, offset lostKGM1_TmpUsr
    .if eax < 4
        mov esi, offset lostKGM1_NoJoy
        mov edi, out_serial
        mov ecx, sizeof lostKGM1_NoJoy
        rep movsb
        Ret
    .endif
    movzx eax, byte ptr ds:[lostKGM1_TmpUsr]
    movzx ebx, byte ptr ds:[lostKGM1_TmpUsr+1]
    sub eax, 60h
    sub ebx, 60h
    imul eax, ebx
    dec eax
    imul eax, eax, 6
    mov RandomCounter, eax

    mov dword ptr ds:[Seed], 7A69h
    mov ecx, RandomCounter
    .while ecx > 0
        invoke msvc_random
        dec ecx
    .endw
    .while ecx < 6
        invoke msvc_random
        mov byte ptr ds:[lostKGM1_FiByte+ecx], al
        inc ecx
    .endw

    ; Let's restore a bit of randomness
    rdtsc
    xor dword ptr ds:[Seed], eax

    invoke lostKGM1_NameMD5Loop, offset lostKGM1_TmpUsr, offset lostKGM1_Hash
    mov eax, dword ptr ds:[lostKGM1_Hash]
    mov dword ptr ds:[lostKGM1_FiByte+6], eax
    mov ax, word ptr ds:[lostKGM1_Hash+4]
    mov word ptr ds:[lostKGM1_FiByte+10], ax

    invoke Base64Encode, offset lostKGM1_FiByte, 12, out_serial
    Ret
Gen_lostitKGM_01 endp

lostKGM1_StripChars proc in_username: DWORD, out_address: DWORD
    local Counter: DWORD
    local OutPosCounter: DWORD

    invoke StrLen, in_username
    mov Counter, eax

    mov OutPosCounter, 0
    xor ecx, ecx
    mov esi, in_username
    mov edi, out_address
    .while ecx < Counter
        mov al, byte ptr ds:[esi+ecx]
        .if al >= 41h && al <= 5Ah
            add al, 20h
        .endif
        .if al >= 61h && al <= 7Ah
            mov edx, OutPosCounter
            mov byte ptr ds:[edi+edx], al
            inc OutPosCounter
        .endif
        inc ecx
    .endw
    Ret
lostKGM1_StripChars endp

lostKGM1_NameMD5Loop proc uses ecx in_bytes: DWORD, out_address: DWORD
    local Counter: DWORD

    invoke StrLen, in_bytes
    mov ebx, eax
    xor ecx, ecx
    mov esi, in_bytes
    mov edi, out_address
    .while ecx < 16
        mov eax, ecx
        cdq
        idiv ebx
        mov al, byte ptr ds:[esi+edx]
        mov byte ptr ds:[edi+ecx], al
        inc ecx
    .endw

    sub ebx, 16
    xor edx, edx
    jmp @f
    @xorloop:
        mov al, byte ptr ds:[esi+edx+16]
        mov ecx, edx
        and ecx, 0Fh
        xor byte ptr ds:[ecx+edi], al
        inc edx
        @@:
        cmp edx, ebx
    jl @xorloop

    mov Counter, 0
    .while Counter < 1000
        invoke MD5Init
        invoke MD5Update, out_address, 16
        invoke MD5Final
        mov ebx, out_address
        xor ecx, ecx
        .while ecx < 4
            mov edx, dword ptr ds:[eax+ecx*4]
            xor dword ptr ds:[ebx+ecx*4], edx
            inc ecx
        .endw
        inc Counter
    .endw
    Ret
lostKGM1_NameMD5Loop endp
