; Tested on SweetScape's Hamic 2.0.1
HamicCRC proto in_username: DWORD, namelen: DWORD

.code
Gen_Hamic proc in_username: DWORD, out_serial: DWORD
    local namelen: DWORD
    local usersnumber: DWORD
    local crc: DWORD
    local primo: DWORD
    local secondo: DWORD
    local terzo: DWORD
    local quarto: DWORD

    invoke StrLen, in_username
    mov namelen, eax
    invoke HamicCRC, in_username, namelen
    mov crc, eax
    xor eax, 4E5301BAh
    imul ecx, eax, 7424BF82h

    mov eax, ecx
    and eax, 0FFFFh
    shr ecx, 16
    xchg al, ah
    xchg cl, ch
    mov cl, 7Fh
    mov primo, eax
    mov secondo, ecx

    mov ecx, crc
    mov eax, ecx
    and eax, 0FFFFh
    shr ecx, 16
    xchg al, ah
    xchg cl, ch
    mov terzo, eax
    mov quarto, ecx

    invoke wsprintf, out_serial, offset Ham_SerialFormat, primo, secondo, terzo, quarto
    Ret
Gen_Hamic endp

HamicCRC proc in_username: DWORD, namelen: DWORD
    local letter: dword
    local crc: dword

    xor eax, eax
    mov crc, eax
    xor ecx, ecx
    xor edx, edx

    .while ecx < namelen
        push ecx
        mov eax, in_username
        mov edx, ecx
        movsx ecx, byte ptr ds:[eax+edx]
        mov letter, ecx
        mov eax, letter
        mov edx, dword ptr ds:[eax*4+HamicTbl]
        add edx, crc
        mov ecx, letter
        add ecx, 0Dh
        and ecx, 0FFh
        xor edx, dword ptr ds:[ecx*4+HamicTbl]
        mov eax, letter
        add eax, 2Fh
        and eax, 0FFh
        imul edx, dword ptr ds:[eax*4+HamicTbl]
        pop ecx
        push ecx
        imul ecx, ecx, 13h
        and ecx, 0FFh
        add edx, dword ptr ds:[ecx*4+HamicTbl]
        mov crc, edx
        pop ecx
        inc ecx
    .endw

    mov eax, crc
    ret
HamicCRC endp
