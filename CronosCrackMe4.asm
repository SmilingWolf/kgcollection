.code
Gen_CronosCrackMe4 proc in_username: DWORD, out_serial: DWORD
    local NameLen: DWORD
    local hFile: DWORD
    local BytesWritten: DWORD

    mov eax, out_serial
    mov byte ptr ds:[eax], 95h
    mov dword ptr ds:[eax+1], 0ED04CD24h
    mov dword ptr ds:[eax+5], 4756ED32h
    invoke StrLen, in_username
    .if eax > 19h
        mov eax, 19h
    .endif
    mov NameLen, eax
    mov ecx, eax
    mov esi, in_username
    mov edi, out_serial
    add edi, 9
    rep movsb

    mov eax, out_serial
    xor ebx, ebx
    xor ecx, ecx
    .while ecx < 9
        add ebx, dword ptr ds:[eax]
        add eax, 4
        inc ecx
    .endw
    not ebx
    inc ebx
    rol ebx, 8
    mov eax, out_serial
    add eax, 23h
    mov dword ptr ds:[eax], ebx
    invoke CreateFile, offset CroCM4_FileName, GENERIC_WRITE, NULL,NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_HIDDEN, NULL
    mov hFile, eax
    invoke WriteFile, hFile, out_serial, 27h, addr BytesWritten, NULL
    invoke CloseHandle, hFile
    Ret
Gen_CronosCrackMe4 endp
