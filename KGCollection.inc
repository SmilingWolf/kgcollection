include         windows.inc

uselib  MACRO   libname
    include     libname.inc
    includelib  libname.lib
ENDM

uselib          user32
uselib          kernel32
uselib          advapi32
uselib          gdi32
uselib          comctl32
uselib          masm32
uselib          zlibstatic
uselib          winmm
uselib          ufmod
uselib          About
uselib          cryptohash
uselib          bignum

DlgProc         PROTO :DWORD,:DWORD,:DWORD,:DWORD

IDI_ICON1       equ     103
RC_XMTRACK      equ     104
IDC_USERNAME    equ     40000
IDC_EXTRA       equ     40013
IDC_SERIAL      equ     40002
BTN_EXIT        equ     40006
STC_TITLE       equ     40008
BTN_GEN         equ     40009
CBB_KGLIST      equ     40011
BTN_ABOUT       equ     40012

.data
Selected_Keygen dd      00h
InitUserName    db      "SmilingWolf", 00h
wText           db      "SmilingWolf's Keygens Collection", 00h
NameTooShort    db      "Username and/or extra data are too short", 00h
KeyfileGen      db      "Keyfile generated!", 00h
cbbCommSoft     db      "--- Commercial Software ---", 00h
cbbKeylogDetect db      "BlazingTools Keylogger Detector 1.36", 00h
cbbPerfKeylog   db      "BlazingTools Perfect Keylogger 1.68.2", 00h
cbbBossInvis    db      "BlazingTools Boss Invisible 1.02", 00h
cbbAdvPDFTools  db      "VeryPDF Advanced PDF Tools 2.0", 00h
cbbHTMLConv     db      "VeryPDF HTML Converter 2.0", 00h
cbbPDFPassRem   db      "VeryPDF PDF Password Remover 6.0", 00h
cbbRLPack       db      "Reversing Labs RLPack 1.21 FULL", 00h
cbb010EditorS   db      "SweetScape 010 Editor 6.0.3", 00h
cbbHamic        db      "SweetScape Hamic 2.0.1", 00h
cbb010Memorizer db      "SweetScape 010 Memorizer 1.1", 00h
cbbMayuraDraw   db      "Mayura Software Mayura Draw 4.5", 00h
cbbWatcher      db      "Digi-Watcher.com Watcher 2.35", 00h
cbbWinCHMPro    db      "Softany WinCHM Pro 5.11", 00h
cbbWordToHelp   db      "Softany WordToHelp 3.13", 00h
cbbCHM2PDFConv  db      "Softany CHM to PDF converter 3.07", 00h
cbbCHM2DOCConv  db      "Softany CHM to DOC converter 3.07", 00h
cbbVMWWorkPro   db      "VMWare Workstation Pro 12.1.1", 00h
cbbFolderLock   db      "NewSoftwares.net Folder Lock 7.6.0", 00h
cbbVideoConvU   db      "Xilisoft Video Converter Ultimate 7.8.19", 00h
cbbDVDRipperUSE db      "Xilisoft DVD Ripper Ultimate SE 7.8.19", 00h
cbbYTVideoConv  db      "Xilisoft YouTube Video Converter 5.6.7", 00h
cbbWebCruiser   db      "Janusec WebCruiser 3.5.5", 00h
cbbWinImage     db      "Gilles Vollant Software WinImage 9.00", 00h
cbbSmartVersion db      "Gilles Vollant Software SmartVersion 3.60", 00h
cbbZIPPassRefix db      "iSumsoft ZIP Password Refixer 3.1.1", 00h
cbbArdamaxKeylo db      "Ardamax Keylogger 4.7", 00h
cbbCrackmes     db      "--- Crackmes/Keygenmes ---", 00h
cbbArTeamCM1    db      "Eggi's ArTeam Crackme #1", 00h
cbbEvOCM1       db      "EvOlUtIoN's CM1", 00h
cbbjjCM         db      "jj89XD's Crackme", 00h
cbbWoot332KGM   db      "Woot332's KeygenMe", 00h
cbbmmUpkMeKgmMe db      "mm10121991's UnpackMe+KeygenMe", 00h
cbbbpx_BM1      db      "bpx_'s BustMe #1", 00h
cbbbpx_BM2      db      "bpx_'s BustMe #2", 00h
cbbOJFunKGM2    db      "Office Jesus' Fun KeygenMe #2", 00h
cbbbartNinjaCM  db      "bart/xt's Ninja CrackMe", 00h
cbbbartPRNGCM   db      "bart/xt's PRNG CrackMe", 00h
cbbThigoLilKM   db      "Thigo's *little* KeygenMe", 00h
cbbTMGOfficKM2  db      "TMG's Official KeygenMe [No.2]", 00h
cbbTMGOfficKM3  db      "TMG's Official KeygenMe [No.3]", 00h
cbbcuPSpliSpla  db      "+cuPegasus' SplishSplash CrackMe", 00h
cbbCruddKrypto  db      "Crudd's Kryptonite KeygenMe", 00h
cbbCronosCM4    db      "Cronos' CrackMe 4", 00h
cbbHappyTowCM10 db      "HappyTown's CrackMe_0010", 00h
cbbHappyTowCM31 db      "HappyTown's CrackMe_0031", 00h
cbbHappyTowCM34 db      "HappyTown's CrackMe_0034", 00h
cbbOROffKGMH    db      "ORiON's Official KeygenMe - Hard", 00h
cbbCyChakraKGM  db      "Cyclops' Chakravyuha KeygenMe", 00h
cbbMagiKAntOdbg db      "MagiK's Anti - OllyDbg 1.0", 00h
cbbREDCrewKGM   db      "RED Crew's Official KeygenMe #1", 00h
cbbx15orBrokLan db      "x15or's Broken Lands KeygenMe", 00h
cbbNeoN192bits  db      "NeoN's 192bits CrackMe", 00h
cbbXyliBBQMe    db      "Xylitol's BarbecueMe", 00h
cbbHMXAliveKGM  db      "HMX0101's Alive KeygenMe", 00h
cbblostitKGM1   db      "lostit's KeygenMe 01", 00h

.data?
hInstance       dd      ?
colour          dd      ?
hpen            dd      ?
sBtnText        TCHAR   50 dup(?)
UserName        dd      50 dup(?)
Extra           dd      50 dup(?)
Serial          dd      50 dup(?)
