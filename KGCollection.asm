.686
.model  flat, stdcall
option  casemap :none   ; case sensitive

include Tables.inc
include Common.inc
include Common.asm
include KGCollection.inc
include BWT.asm
include KeyloggerDetector.asm
include PerfectKeylogger.asm
include BossInvisible.asm
include AdvancedPDFTools.asm
include HTMLConverter.asm
include PDFPasswordRemover.asm
include RLPack.asm
include 010Editor.asm
include Hamic.asm
include 010Memorizer.asm
include MayuraDraw.asm
include Watcher.asm
include WinCHMPro.asm
include WordToHelp.asm
include CHM2PDF.asm
include CHM2DOC.asm
include VMWareWorkstationPro.asm
include FolderLock.asm
include VideoConverterUltimate.asm
include DVDRipperUltimateSE.asm
include YouTubeVideoConverter.asm
include WebCruiser.asm
include WinImage.asm
include SmartVersion.asm
include ZIPPasswordRefixer.asm
include ArdamaxKeylogger.asm
include ArTeamCrackme#1.asm
include EvOlUtIoNCM1.asm
include jj89XDCrackme.asm
include Woot332KeygenMe.asm
include mmUnpackmeKeygenMe.asm
include bpx_BustMe#1.asm
include bpx_BustMe#2.asm
include OJFunKGM2.asm
include bartNinjaCrackMe.asm
include bartPRNGCrackMe.asm
include ThigoLilKeygenMe.asm
include TMGOfficialKM2.asm
include TMGOfficialKM3.asm
include cuPegasusSplishSplash.asm
include CruddKryptonite.asm
include CronosCrackMe4.asm
include HappyTownCrackMe_10.asm
include HappyTownCrackMe_31.asm
include HappyTownCrackMe_34.asm
include ORiONOfficialKGMHard.asm
include CyclopsChakravyuhaKGM.asm
include MagiKAntiODbg.asm
include REDCrewOfficialKGM1.asm
include x15orBrokenLandsKGM.asm
include NeoN192bitsCrackMe.asm
include XylitolBarbecueMe.asm
include HMX0101AliveKeygenMe.asm
include lostitKGM_01.asm

AboutDlg proto hWin: DWORD, uMsg: DWORD, wParam: DWORD, lParam: DWORD
DrawItem proto hWin: HWND, lParam: LPARAM

.code
start:
    invoke InitCommonControls
    invoke CreateSolidBrush, 0
    mov colour, eax
    invoke CreatePen, PS_SOLID, 0, 00h
    mov hpen, eax
    invoke uFMOD_PlaySong, RC_XMTRACK, 0, XM_RESOURCE
    invoke  GetModuleHandle, NULL
    mov hInstance, eax
    invoke  DialogBoxParam, hInstance, 100, 0, ADDR DlgProc, 0
    invoke  ExitProcess, eax
    ret
; -----------------------------------------------------------------------
DlgProc proc hWin :DWORD, uMsg :DWORD, wParam :DWORD, lParam :DWORD
    local NameLen: DWORD
    local ExtraLen: DWORD

    .if uMsg == WM_INITDIALOG
        invoke LoadIcon, hInstance, IDI_ICON1
        invoke SendMessage, hWin, WM_SETICON, 1, eax
        invoke SetDlgItemText, hWin, IDC_USERNAME, offset InitUserName
        invoke SetDlgItemText, hWin, STC_TITLE, offset wText
        invoke SetWindowText, hWin, offset wText
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCommSoft
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbKeylogDetect
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbPerfKeylog
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbBossInvis
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbAdvPDFTools
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbHTMLConv
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbPDFPassRem
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbRLPack
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbb010EditorS
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbHamic
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbb010Memorizer
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbMayuraDraw
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbWatcher
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbWinCHMPro
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbWordToHelp
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCHM2PDFConv
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCHM2DOCConv
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbVMWWorkPro
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbFolderLock
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbVideoConvU
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbDVDRipperUSE
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbYTVideoConv
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbWebCruiser
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbWinImage
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbSmartVersion
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbZIPPassRefix
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbArdamaxKeylo
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCrackmes
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbArTeamCM1
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbEvOCM1
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbjjCM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbWoot332KGM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbmmUpkMeKgmMe
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbbpx_BM1
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbbpx_BM2
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbOJFunKGM2
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbbartNinjaCM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbbartPRNGCM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbThigoLilKM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbTMGOfficKM2
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbTMGOfficKM3
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbcuPSpliSpla
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCruddKrypto
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCronosCM4
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbHappyTowCM10
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbHappyTowCM31
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbHappyTowCM34
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbOROffKGMH
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbCyChakraKGM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbMagiKAntOdbg
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbREDCrewKGM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbx15orBrokLan
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbNeoN192bits
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbXyliBBQMe
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbbHMXAliveKGM
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_ADDSTRING, 0, addr cbblostitKGM1
        invoke SendDlgItemMessage, hWin, CBB_KGLIST, CB_SETCURSEL, 0, 0
    .elseif uMsg == WM_DRAWITEM
        invoke DrawItem, hWin, lParam
    .elseif uMsg == WM_CTLCOLORDLG || uMsg == WM_CTLCOLOREDIT || uMsg == WM_CTLCOLORSTATIC
        invoke SetBkMode, wParam, TRANSPARENT
        invoke SetBkColor, wParam, 0
        invoke SetTextColor, wParam, 00FFFFFFh
        mov eax, colour
        ret
    .elseif uMsg == WM_LBUTTONDOWN
        invoke SendMessage, hWin, WM_NCLBUTTONDOWN, HTCAPTION, 0
    .elseif uMsg == WM_COMMAND
        movzx eax, word ptr wParam
        .if eax == BTN_EXIT
            invoke EndDialog, hWin, 0
        .elseif eax == BTN_GEN
        ;--------HERE--------;
            rdtsc
            xor dword ptr ds:[Seed], eax
            invoke RtlZeroMemory, offset UserName, sizeof UserName
            invoke RtlZeroMemory, offset Extra, sizeof Extra
            invoke RtlZeroMemory, offset Serial, sizeof Serial
            invoke GetDlgItemText, hWin, IDC_USERNAME, offset UserName, 200
            mov NameLen, eax
            invoke GetDlgItemText, hWin, IDC_EXTRA, offset Extra, 200
            mov ExtraLen, eax
            mov eax, Selected_Keygen
            .if eax == 1
                invoke Gen_KeyloggerDetector, offset UserName, offset Serial
            .elseif eax == 2
                invoke Gen_PerfectKeylogger, offset UserName, offset Serial
            .elseif eax == 3
                invoke Gen_BossInvisible, offset UserName, offset Serial
            .elseif eax == 4
                invoke Gen_AdvancedPDFTools, offset Serial
            .elseif eax == 5
                invoke Gen_HTMLConverter, offset Serial
            .elseif eax == 6
                invoke Gen_PDFPasswordRemover, offset Serial
            .elseif eax == 7
                invoke Gen_RLPack, offset UserName, offset Serial
                mov esi, offset KeyfileGen
                mov edi, offset Serial
                mov ecx, sizeof KeyfileGen
                rep movsb
            .elseif eax == 8
                invoke Gen_010Editor, offset UserName, offset Serial
            .elseif eax == 9
                invoke Gen_Hamic, offset UserName, offset Serial
            .elseif eax == 10
                invoke Gen_010Memorizer, offset UserName, offset Serial
            .elseif eax == 11
                invoke Gen_MayuraDraw, offset UserName, offset Serial
            .elseif eax == 12
                invoke Gen_Watcher, offset UserName, offset Serial
            .elseif eax == 13
                invoke Gen_WinCHMPro, offset Serial
            .elseif eax == 14
                invoke Gen_WordToHelp, offset Serial
            .elseif eax == 15
                invoke Gen_CHM2PDF, offset Serial
            .elseif eax == 16
                invoke Gen_CHM2DOC, offset Serial
            .elseif eax == 17
                invoke Gen_VMWareWorkstationPro, offset Serial
            .elseif eax == 18
                invoke Gen_FolderLock, offset UserName, offset Serial
            .elseif eax == 19
                invoke Gen_VideoConverterUltimate, offset Serial
            .elseif eax == 20
                invoke Gen_DVDRipperUltimateSE, offset Serial
            .elseif eax == 21
                invoke Gen_YouTubeVideoConverter, offset Serial
            .elseif eax == 22
                invoke Gen_WebCruiser, offset UserName, offset Serial
            .elseif eax == 23
                invoke Gen_WinImage, offset UserName, offset Serial
            .elseif eax == 24
                invoke Gen_SmartVersion, offset UserName, offset Serial
            .elseif eax == 25
                invoke Gen_ZIPPasswordRefixer, offset Serial
            .elseif eax == 26
                invoke Gen_ArdamaxKeylogger, offset Serial
            .elseif eax == 28
                invoke Gen_ArTeamCrackme1, offset Serial
            .elseif eax == 29
                .if NameLen < 4
                    mov esi, offset NameTooShort
                    mov edi, offset Serial
                    mov ecx, sizeof NameTooShort
                    rep movsb
                .else
                    invoke Gen_EvOlUtIoNCM1, offset UserName, offset Serial
                .endif
            .elseif eax == 30
                invoke Gen_jj89XDCrackme, offset UserName, offset Serial
            .elseif eax == 31
                invoke Gen_Woot332KeygenMe, offset UserName, offset Serial
            .elseif eax == 32
                mov NameLen, 200
                invoke GetComputerName, offset Extra, addr NameLen
                invoke SetDlgItemText, hWin, IDC_EXTRA, offset Extra
                invoke Gen_mmUnpackmeKeygenMe, offset Extra, offset Serial
            .elseif eax == 33
                invoke Gen_bpx_BustMe1, offset UserName, offset Serial
            .elseif eax == 34
                invoke Gen_bpx_BustMe2, offset UserName, offset Serial
            .elseif eax == 35
                .if NameLen < 5
                    mov esi, offset NameTooShort
                    mov edi, offset Serial
                    mov ecx, sizeof NameTooShort
                    rep movsb
                .else
                    invoke Gen_OJFunKGM2, offset UserName, offset Serial
                .endif
            .elseif eax == 36
                invoke Gen_bartNinjaCrackMe, offset UserName, offset Serial
            .elseif eax == 37
                invoke Gen_bartPRNGCrackMe, offset UserName, offset Extra, offset Serial
            .elseif eax == 38
                invoke Gen_ThigoLilKeygenMe, offset UserName, offset Serial
                mov esi, offset KeyfileGen
                mov edi, offset Serial
                mov ecx, sizeof KeyfileGen
                rep movsb
            .elseif eax == 39
                .if NameLen < 5 || ExtraLen < 5
                    mov esi, offset NameTooShort
                    mov edi, offset Serial
                    mov ecx, sizeof NameTooShort
                    rep movsb
                .else
                    invoke Gen_TMGOfficialKM2, offset UserName, offset Extra, offset Serial
                .endif
            .elseif eax == 40
                .if NameLen < 5
                    mov esi, offset NameTooShort
                    mov edi, offset Serial
                    mov ecx, sizeof NameTooShort
                    rep movsb
                .else
                    invoke Gen_TMGOfficialKM3, offset UserName, offset Serial
                .endif
            .elseif eax == 41
                invoke Gen_cuPegasusSplishSplash, offset UserName, offset Serial
            .elseif eax == 42
                invoke Gen_CruddKryptonite, offset UserName, offset Serial
            .elseif eax == 43
                invoke Gen_CronosCrackMe4, offset UserName, offset Serial
                mov esi, offset KeyfileGen
                mov edi, offset Serial
                mov ecx, sizeof KeyfileGen
                rep movsb
            .elseif eax == 44
                invoke Gen_HappyTownCrackMe_10, offset UserName, offset Serial
            .elseif eax == 45
                invoke Gen_HappyTownCrackMe_31, offset UserName, offset Serial
            .elseif eax == 46
                invoke Gen_HappyTownCrackMe_34, offset UserName, offset Extra, offset Serial
            .elseif eax == 47
                invoke Gen_ORiONOfficialKGMHard, offset UserName, offset Serial
            .elseif eax == 48
                invoke Gen_CyclopsChakravyuhaKGM, offset UserName, offset Serial
            .elseif eax == 49
                .if ExtraLen == 00h
                    mov esi, offset NameTooShort
                    mov edi, offset Serial
                    mov ecx, sizeof NameTooShort
                    rep movsb
                .else
                    invoke Gen_MagiKAntiODbg, offset UserName, offset Extra, offset Serial
                .endif
            .elseif eax == 50
                invoke Gen_REDCrewOfficialKGM1, offset UserName, offset Serial
            .elseif eax == 51
                invoke Gen_x15orBrokenLandsKGM, offset UserName, offset Serial
            .elseif eax == 52
                invoke Gen_NeoN192bitsCrackMe, offset UserName, offset Serial
            .elseif eax == 53
                invoke Gen_XylitolBarbecueMe, offset UserName, offset Serial
            .elseif eax == 54
                invoke Gen_HMX0101AliveKeygenMe, offset UserName, offset Serial
            .elseif eax == 55
                .if NameLen < 5
                    mov esi, offset NameTooShort
                    mov edi, offset Serial
                    mov ecx, sizeof NameTooShort
                    rep movsb
                .else
                    invoke Gen_lostitKGM_01, offset UserName, offset Serial
                .endif
            .endif
            invoke SetDlgItemText, hWin, IDC_SERIAL, offset Serial
        ;--------HERE--------;
        .elseif eax == BTN_ABOUT
            invoke  DialogBoxParam, hInstance, 200, hWin, ADDR AboutDlg, 0
        .elseif eax == CBB_KGLIST
            movzx eax, word ptr wParam+2
            .if eax == CBN_SELCHANGE
                movzx eax, word ptr wParam
                invoke SendDlgItemMessage, hWin, eax, CB_GETCURSEL, 0, 0
                mov Selected_Keygen, eax
            .endif
        .endif
    .elseif uMsg == WM_CLOSE
        invoke uFMOD_PlaySong, 0, 0, 0
        invoke EndDialog, hWin, 0
    .endif

    xor eax, eax
    ret
DlgProc endp

AboutDlg proc hWin :DWORD, uMsg :DWORD, wParam :DWORD, lParam :DWORD
    .if uMsg == WM_INITDIALOG
        invoke AboutCreate, hWin, 0, 0, 360, 290
    .elseif uMsg == WM_LBUTTONDOWN
        invoke SendMessage, hWin, WM_NCLBUTTONDOWN, HTCAPTION, 0
    .elseif uMsg == WM_RBUTTONUP
        invoke SendMessage, hWin, WM_CLOSE, 0, 0
    .elseif uMsg == WM_CLOSE
        invoke EndDialog, hWin, 0
    .endif

    xor eax, eax
    ret
AboutDlg endp

DrawItem proc uses esi hWin: HWND, lParam: LPARAM
    mov esi, lParam
    assume esi: ptr DRAWITEMSTRUCT

    invoke SelectObject, [esi].hdc, colour
    invoke SelectObject, [esi].hdc, hpen

    invoke FillRect, [esi].hdc, ADDR [esi].rcItem, hpen
    invoke Rectangle, [esi].hdc, [esi].rcItem.left, [esi].rcItem.top, [esi].rcItem.right, [esi].rcItem.bottom

    .if [esi].itemState & ODS_SELECTED
        invoke OffsetRect, ADDR [esi].rcItem, 1, 1
    .endif

    invoke GetDlgItemText, hWin, [esi].CtlID, ADDR sBtnText, SIZEOF sBtnText
    invoke SetBkMode, [esi].hdc, TRANSPARENT
    invoke SetTextColor, [esi].hdc, 0FFFFFFh
    invoke DrawText, [esi].hdc, ADDR sBtnText, -1, ADDR [esi].rcItem, DT_CENTER or DT_VCENTER or DT_SINGLELINE

    .if [esi].itemState & ODS_SELECTED
        invoke OffsetRect, ADDR [esi].rcItem, -1, -1
    .endif

    .if [esi].itemState & ODS_DISABLED
        invoke GetDlgItemText, hWin, [esi].CtlID, ADDR sBtnText, SIZEOF sBtnText
        invoke SetBkMode, [esi].hdc, TRANSPARENT
        invoke SetTextColor, [esi].hdc, 099A8ACh
        invoke DrawText, [esi].hdc, ADDR sBtnText, -1, ADDR [esi].rcItem, DT_CENTER or DT_VCENTER or DT_SINGLELINE
    .endif

    assume esi:nothing
    mov eax, TRUE
    Ret
DrawItem endp

end start
