.code
Gen_HappyTownCrackMe_31 proc in_username: DWORD, out_serial: DWORD
    local uno: DWORD
    local due: DWORD
    local tre: DWORD
    local quattro: DWORD

    invoke TigerInit
    mov ebx, in_username
    invoke TigerUpdate, ebx, 1
    invoke TigerFinal
    mov eax, dword ptr ds:[eax]
    bswap eax
    mov uno, eax

    invoke TigerInit
    mov ebx, in_username
    inc ebx
    invoke TigerUpdate, ebx, 1
    invoke TigerFinal
    mov eax, dword ptr ds:[eax]
    bswap eax
    mov due, eax

    invoke TigerInit
    mov ebx, in_username
    inc ebx
    inc ebx
    invoke TigerUpdate, ebx, 1
    invoke TigerFinal
    mov eax, dword ptr ds:[eax]
    bswap eax
    mov tre, eax

    invoke TigerInit
    invoke StrLen, in_username
    mov ebx, in_username
    invoke TigerUpdate, ebx, eax
    invoke TigerFinal
    mov eax, dword ptr ds:[eax]
    bswap eax
    mov quattro, eax

    invoke wsprintf, out_serial, offset HTCM31_SFormat, uno, due, tre, quattro
    Ret
Gen_HappyTownCrackMe_31 endp
