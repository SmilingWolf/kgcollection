; Tested on VeryPDF Advanced PDF Tools 2.0

.code
Gen_AdvancedPDFTools proc out_serial: DWORD
    local secondo: DWORD
    local nono: DWORD
    local dodicesimo: DWORD

    .repeat
        invoke random, 9h
        mov edx, eax
    .until dl != 0h

    mov byte ptr [secondo], dl
    mov eax, 12h
    sub eax, edx
    mov ecx, 2h
    xor edx, edx
    div ecx
    mov byte ptr [nono], al
    mov eax, 12h
    sub al, byte ptr [secondo]
    sub al, byte ptr [nono]
    mov byte ptr [dodicesimo], al

    add byte ptr [secondo], 30h
    add byte ptr [nono], 30h
    add byte ptr [dodicesimo], 30h

    xor ebx, ebx
    mov edi, out_serial
    .while ebx < 10h
        invoke random, 9h
        mov edx, eax
        add dl, 30h
        mov byte ptr [edi+ebx], dl
        inc ebx
    .endw

    mov eax, out_serial
    mov dl, byte ptr [secondo]
    mov byte ptr [eax+1h], dl
    mov dl, byte ptr [nono]
    mov byte ptr [eax+8h], dl
    mov dl, byte ptr [dodicesimo]
    mov byte ptr [eax+0Bh], dl
    Ret
Gen_AdvancedPDFTools endp
