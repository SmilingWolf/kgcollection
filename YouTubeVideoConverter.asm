; Tested on Xilisoft YouTube Video Converter 5.6.7

.code
Gen_YouTubeVideoConverter proc out_serial: DWORD
    invoke RtlZeroMemory, offset YTViCon_TmpStr, sizeof YTViCon_TmpStr

    xor ecx, ecx
    mov esi, offset YTViCon_Alpha
    mov edi, out_serial
    .while ecx < 19
        invoke random, 16
        movzx eax, byte ptr ds:[esi+eax]
        mov byte ptr ds:[edi+ecx], al
        inc ecx
    .endw
    mov byte ptr ds:[edi+4], "-"
    mov byte ptr ds:[edi+9], "-"
    mov byte ptr ds:[edi+14], "-"
    mov byte ptr ds:[edi+19], "-"
    mov ecx, 20
    mov esi, out_serial
    mov edi, offset YTViCon_Magic+63
    rep movsb
    invoke MD5Init
    invoke MD5Update, offset YTViCon_Magic, 113
    invoke MD5Final
    xor ecx, ecx
    mov edi, offset YTViCon_TmpStr
    .while ecx < MD5_DIGESTSIZE
        movzx ebx, byte ptr ds:[eax+ecx]
        and ebx, 0F0h
        movzx edx, byte ptr ds:[eax+ecx+1]
        shr edx, 4
        or ebx, edx
        mov byte ptr ds:[edi], bl
        add ecx, 2
        inc edi
    .endw
    mov ax, word ptr ds:[YTViCon_TmpStr]
    bswap eax
    shr eax, 16
    mov bx, word ptr ds:[YTViCon_TmpStr+2]
    bswap ebx
    shr ebx, 16
    mov cx, word ptr ds:[YTViCon_TmpStr+4]
    bswap ecx
    shr ecx, 16
    mov dx, word ptr ds:[YTViCon_TmpStr+6]
    bswap edx
    shr edx, 16
    mov edi, out_serial
    add edi, 20
    invoke wsprintf, edi, offset YTViCon_SerForm, eax, ebx, ecx, edx
    Ret
Gen_YouTubeVideoConverter endp
