; Fuckton of crypto and magic here!
; I don't know if it was intentional or not, but the DLP problem
; to solve had a 128bit long P. Now, let's set 2 things straight:
; 1) nobody gives a crap about solving DLPs, which means the software
; to complete such a task is rare and immature;
; 2) compared to integer factorization, solving a DLP is fucking
; harder! Factoring a 128bit RSA modulus -> easy, solving a DLP
; with a 128bit prime modulus -> much harder.
; It took 18943.97 seconds (around 5.26 hours) to solve the problem
; on my lil' laptop using dlog (https://github.com/onechip/dlog).
; A really poor choice for a simple Trial KeygenMe if you ask me.
; Anyway, everyting else was pretty nice. MD5 + SHA1 + CRC32b + custom
; math to get H(m) to feed to ElGamal signing, RSA to encrypt R and S,
; transliteration of the serial... lots of things and overall fun.
; One small note: since the ElGamal modulus is bigger than both RSA
; moduli there are some K that will generate R and S bigger than RSA's
; N. To do RSA encryption we need 0 <= m < n and gcd(m, n) = 1,
; where m will be the part of the signature we're dealing with at the
; moment (either R or S) and n is of course the current N (two different
; moduli are used, one to encrypt R and another for S).
; This said, it appeared to me that not all K will give acceptable R and S.
; I solved the problem simply by generating a new K and a new signature.

.code
Gen_REDCrewOfficialKGM1 proc in_username: DWORD, out_serial: DWORD
    local NameSum: DWORD

	invoke RtlZeroMemory, offset REDCKGM_NSumStr, sizeof REDCKGM_NSumStr
	invoke RtlZeroMemory, offset REDCKGM_TempStr, sizeof REDCKGM_TempStr

    invoke MD5Init
    invoke StrLen, in_username
    invoke MD5Update, in_username, eax
    invoke MD5Final
    invoke HexEncode, eax, MD5_DIGESTSIZE, offset REDCKGM_TempStr
    invoke SHA1Init
    invoke StrLen, offset REDCKGM_TempStr
    invoke SHA1Update, offset REDCKGM_TempStr, eax
    invoke SHA1Final
    invoke CalcCRC32b, eax, SHA1_DIGESTSIZE
    mov NameSum, eax
    invoke HexEncode, addr NameSum, 4, offset REDCKGM_TempStr
    mov esi, offset REDCKGM_TempStr
    mov ecx, 8
    add esi, ecx
    inc esi
    not ecx
    xor ebx, ebx
    .while ecx > 0
        movzx eax, byte ptr ds:[esi+ecx]
        add eax, ebx
        add eax, ebx
        mov ebx, eax
        inc ecx
    .endw
    shr eax, 1
    bswap eax
    mov NameSum, eax
    invoke HexEncode, addr NameSum, 4, offset REDCKGM_NSumStr

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigTmp, eax
    invoke bnCreate
    mov BigM, eax
    Invoke bnCreate
    mov BigP, eax
    invoke bnCreate
    mov BigK, eax
    invoke bnCreate
    mov BigG, eax
    invoke bnCreate
    mov BigR, eax
    invoke bnCreate
    mov BigS, eax
    invoke bnCreate
    mov BigX, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigD, eax

    @@:
    invoke bnClear, BigTmp
    invoke bnClear, BigM
    invoke bnClear, BigP
    invoke bnClear, BigK
    invoke bnClear, BigG
    invoke bnClear, BigR
    invoke bnClear, BigS
    invoke bnClear, BigX

    invoke bnFromHex, offset REDCKGM_NSumStr, BigM
    invoke bnFromHex, offset REDCKGM_P, BigP

    invoke bnRsaGenPrime, BigK, 96
    invoke bnFromHex, offset REDCKGM_G, BigG
    invoke bnModExp, BigG, BigK, BigP, BigR
    invoke bnDec, BigP

    invoke bnFromHex, offset REDCKGM_X, BigX
    invoke bnModInv, BigK, BigP, BigK
    invoke bnAdd, BigM, BigP
    invoke bnMul, BigX, BigR, BigTmp
    invoke bnMod, BigTmp, BigP, BigTmp
    invoke bnSub, BigM, BigTmp
    invoke bnMod, BigM, BigP, BigTmp
    invoke bnMul, BigTmp, BigK, BigTmp
    invoke bnMod, BigTmp, BigP, BigS

    invoke bnClear, BigN
    invoke bnClear, BigD
    invoke bnClear, BigM
    invoke bnFromHex, offset REDCKGM_NR, BigN
    invoke bnCmp, BigR, BigN
    cmp eax, -1
    jne @b
    invoke bnGCD, BigR, BigN, BigTmp
    mov eax, BigTmp
    cmp dword ptr ds:[eax], 01
    jne @b
    add eax, 08h
    cmp dword ptr ds:[eax], 01
    jne @b

    invoke bnFromHex, offset REDCKGM_DR, BigD
    invoke bnModExp, BigR, BigD, BigN, BigM
    invoke bnToHex, BigM, offset REDCKGM_TempStr

    invoke bnClear, BigN
    invoke bnClear, BigD
    invoke bnClear, BigM
    invoke bnFromHex, offset REDCKGM_NS, BigN
    invoke bnCmp, BigS, BigN
    cmp eax, -1
    jne @b
    invoke bnGCD, BigS, BigN, BigTmp
    mov eax, BigTmp
    cmp dword ptr ds:[eax], 01
    jne @b
    add eax, 08h
    cmp dword ptr ds:[eax], 01
    jne @b

    invoke bnFromHex, offset REDCKGM_DS, BigD
    invoke bnModExp, BigS, BigD, BigN, BigM

    invoke StrLen, offset REDCKGM_TempStr
    add eax, offset REDCKGM_TempStr
    mov byte ptr ds:[eax], "-"
    inc eax
    invoke bnToHex, BigM, eax

    invoke bnDestroy, BigP
    invoke bnDestroy, BigK
    invoke bnDestroy, BigG
    invoke bnDestroy, BigS
    invoke bnDestroy, BigX
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnDestroy, BigM
    invoke bnDestroy, BigTmp
    invoke bnDestroy, BigR
    invoke bnFinish

    xor esi, esi
    movzx eax, byte ptr ds:[REDCKGM_TempStr]
    .while eax != 00h
        mov edi, offset REDCKGM_AlphaFr
        mov ecx, sizeof REDCKGM_AlphaFr
        repne scasb
        sub ecx, sizeof REDCKGM_AlphaFr
        not ecx
        movzx eax, byte ptr ds:[REDCKGM_AlphaTo+ecx]
        mov ebx, out_serial
        mov byte ptr ds:[ebx+esi], al
        inc esi
        movzx eax, byte ptr ds:[REDCKGM_TempStr+esi]
    .endw
    Ret
Gen_REDCrewOfficialKGM1 endp
